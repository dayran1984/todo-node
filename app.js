const argv = require('./config/yargs').argv;
const toDo = require('./to-do/to-do');
const colors = require('colors');

let command = argv._[0];

switch (command) {
    case 'create':
        console.log(toDo.create(argv.description));
        break;
    case 'list':
        let listToDo = toDo.list();
        for (let task of listToDo) {
            console.log('========To Do========'.green);
            console.log('Description:',task.description);
            console.log('Completed:', task.completed );
            console.log('====================='.green);
        }
        break;
    case 'update':
        let updated = toDo.update(argv.description,argv.completed);
        console.log(updated);
        break;
    case 'delete':
        let deleted = toDo.deleteTask(argv.description);
        console.log(deleted);
        break;
    default:
    console.log('This command do not exist');
        break;
}