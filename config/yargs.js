const description = {
    demand: true,
    alias: 'd',
   desc: 'Description about this task'
};

const completed = {
    default: true,
    alias: 'c',
    desc: 'Mark as completed or pendindg'
};


const argv = require('yargs')
                .command('create','Create an element',{
                    description
                })
                .command('update','Update a complete task',{
                    description,
                    completed
                })
                .command('delete','Delete a task',{
                    description
                })
                .help()
                .argv;

module.exports = {
    argv
}                