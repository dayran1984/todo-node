const fs = require('fs');

let todoList = [];

const saveDB = () => {
    let data = JSON.stringify(todoList);
    fs.writeFile('db/data.json',data,(err)=>{
        if (err) {
            throw new Error('Can not save in the file.',err);
        }
    });
}

const loadDB = () => {
    try {
        todoList = require('../db/data.json');
    } catch (error) {
        todoList =[];
    }
        
}

const create = (description) => {
    
    loadDB();
    
    let todo = {
        description,
        completed: false
    };

    todoList.push(todo);
    saveDB();
    return todo;
}

const list = () => {    
    loadDB();
    return todoList;
}

const update = (description, completed = true) => {
    loadDB();
    let index = todoList.findIndex(task => {
        return task.description === description;
    })
    if (index >= 0) {
        todoList[index].completed = completed;
        saveDB();
        return true;
    }
    else return false;
}

const deleteTask = (description) => {
    loadDB();
    let newToDoList = todoList.filter(task => {
        return task.description !== description;
    })
    if (newToDoList.length !== todoList.length) {        
        todoList = newToDoList;
        saveDB();
        return true;
    }
    else return false;
}


module.exports = {
    create,
    list,
    update,
    deleteTask
}